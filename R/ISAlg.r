library(party)
library(pROC)
library(randomForest)

getTresholdValue = function(prediction, output){
  trainRoc = roc(output, prediction, plot=F)
  trainRocCoords = coords(trainRoc, "best", ret=c("threshold"))
  
  trainTreshhold = as.numeric(trainRocCoords[1])
  return(trainTreshhold)
}


calculateRandomForestTreshold = function(randomForestModel, trainData){
  trainPredict = predict(randomForestModel, newdata=trainData,type="prob")[,1]
  threshold = getTresholdValue(trainPredict, trainData$Presence)
  return(threshold)
}

calculateCTTreeTreshold = function(ctTreeModel, trainData){
  trainPredict = sapply(predict(ctTreeModel, newdata=trainData,type="prob"),'[[',2)  # obtain probability of class 1 (second element from the lists)
  threshold = getTresholdValue(trainPredict, trainData$Presence)
  return(threshold)
}

calculateTreshold = function(model, trainData, output){
  trainPredict = predict(model, trainData, type="response")
  trainTreshhold = getTresholdValue(trainPredict, output)
  return(trainTreshhold)
} 

#' ISAlg Function
#' @export
#' @examples
#' ISAlg()
ISAlg = function(dataRange, data) {
  ctTree = ctree(dataRange, data = data)
  randomForestAlg = randomForest(dataRange, data=data, ntree=50)
  glm = glm(dataRange, data, family = binomial(link = logit))
  
  
  model = structure(list(
    ctTree = ctTree, 
    randomForestAlg = randomForestAlg,
    glm = glm,
    train = data
  ), class = "class") 
  return(model)
}

#' predict.class Function
#' @export
#' @examples
#' predict.class()
predict.class = function(modelObject, newdata) {
  
  #prediction 1
  modelObject$predClass = predict(modelObject$ctTree, newdata=newdata, type="response")    # obtain the class (0/1)
  modelObject$predProb = sapply(predict(modelObject$ctTree, newdata=newdata,type="prob"),'[[',2)  # obtain probability of class 1 (second element from the lists)
  
  ctTreeTreshold = calculateCTTreeTreshold(modelObject$ctTree, modelObject$train)
  modelObject$predClass1[modelObject$predProb < ctTreeTreshold] = 0
  modelObject$predClass1[modelObject$predProb >= ctTreeTreshold] = 1
  
  #prediction 2
  modelObject$predictClassRandomForest = predict(modelObject$randomForestAlg, newdata=newdata)    # obtain the class (0/1)
  modelObject$predProbRandomForest = predict(modelObject$randomForestAlg, newdata=newdata,type="prob")[,1]
  
  forestTreshold = calculateRandomForestTreshold(modelObject$randomForestAlg, modelObject$train)
  
  modelObject$predClass2[modelObject$predProbRandomForest < forestTreshold] = 0
  modelObject$predClass2[modelObject$predProbRandomForest >= forestTreshold] = 1
  
  
  #prediction 3
  modelObject$glmPred = predict(modelObject$glm, newdata = newdata, type="response")
  
  trainTreshhold = calculateTreshold(model = modelObject$glm, trainData = modelObject$train, output = modelObject$train$Presence)
  
  modelObject$predClass3[ modelObject$glmPred < trainTreshhold] = 0
  modelObject$predClass3[ modelObject$glmPred >= trainTreshhold] = 1
  
  predictionsByModels = data.frame(modelObject$predClass1, modelObject$predClass2, modelObject$predClass3)

  modelObject$outputPrediction = rowMeans(predictionsByModels, na.rm = FALSE, dims = 1)
  modelObject$prediction[modelObject$outputPrediction < 0.5] = 0
  modelObject$prediction[modelObject$outputPrediction >= 0.5] = 1
  
  return(modelObject$prediction)
} 

